// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    // 0 1 1 2 3 5 8 13 21 34 55

    if n == 0 {
        return 0;
    } else if n == 1 {
        return 1;
    } else {
        let mut x = 0;
        let mut y = 1;
        let mut t = 0;
        let z = n-1;

        for _i in 0..z {
            t = x + y;
            x = y;
            y = t;
        }

        return t;
    }

}


fn main() {
    println!("{}", fibonacci_number(10));
}
